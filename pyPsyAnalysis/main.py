import numpy as np
import matplotlib.pyplot as plt
import configparser
from collections import OrderedDict
import csv 
import os

def read_csv(filename="file.csv", delimiter=','):
    """
    Reads a csv file
    :param filename: name of file to read.
    """
    f = open(filename, 'r')
    l = f.readlines()
    ls = l[1:]
    hdrs = l[0].split(delimiter)
    data = OrderedDict()
    for i, hdr in enumerate(hdrs):
        data[hdr] = []
        for ll in ls:
            try:
                data[hdr].append(ll.split(delimiter)[i])
            except IndexError:
                data[hdr].append('0')
    f.close()
    return hdrs, data

def write_ini(analysis, filename, username='NOT SPECIFIED', date='NOT SPECIFIED', key='COMPUTATION_001'):
    """
    Write analysis results in ini format
    """
    config = configparser.ConfigParser()
    config['Metadata'] = {}
    config['Metadata']['User'] = username
    config['Metadata']['Date'] = date
    config['Metadata']['Key'] = key
    for i, analysis_i in analysis.items():
        config['Analysis_{}'.format(i.rjust(3,'0'))] = analysis_i
    with open(filename.split('.')[0] + '.ini', 'w') as configfile:
        config.write(configfile)
        
def write_csv(data, filename, append=False, delimiter=','):
    """
    Writes the calculated data to a csv dile
    """
    f = open(filename, 'w', newline='')
    w = csv.writer(f, delimiter=';')
    w.writerow([data[key]['comment'] for key in data.keys()])
    key = list(data.keys())[0]
    n = len(data[key]['results'].split(','))
    for i in range(n):
        #print([data[key]['results'][i] for key in data.keys()])
        w.writerow([data[key]['results'].split(',')[i].replace('\n', '') for key in data.keys()])
    f.close()

class Dataset:
    def __init__(self, filename, delimiter=','):
        """
        Constructor
        """
        self._filename = filename
        self._hdrs = read_csv(self._filename, delimiter=delimiter)[0]
        self._data = read_csv(self._filename, delimiter=delimiter)[1]

    def mean_all_participants(self, columns, analysis=OrderedDict(), id0='1'):
        """
        Compute the mean vaue of a list of columns
        and writes in an output file.
        """
        if isinstance(columns[0], int):
            columns = [self._hdrs[i] for i in columns]
        means = []
        for c in columns:
            means.append(np.mean(np.array(self._data[c])))
        analysis['MEAN_ALL_{}'.format(id0)] = OrderedDict({'name': 'mean_all_participants', 'columns': ','.join(columns), 'results': mean})
        return analysis

    def mean_per_participant(self, columns, analysis=OrderedDict(), comment='', id0='1'):
        """
        Compute the mean value of a list of columns
        """
        if isinstance(columns[0], int):
            columns = [self._hdrs[i] for i in columns]
        means = []
        for i in range(len(self._data[columns[0]])):
            means.append(np.mean(np.array([float(self._data[c][i]) for c in columns]), axis=0))
        analysis['MEAN_PER_{}'.format(id0)] = OrderedDict({'name': 'mean_per_participant', 'columns': ','.join(columns), 'results': ','.join(['{0:.3f}'.format(m).rjust(5) for m in means]), 'comment': comment})
        return analysis
        
    def count_number_of_responses_per_modalities(self, columns, analysis=OrderedDict(), id0='1', comment='', n=4):
        """
        For each column, count the number of responses per modalities.
        """
        if isinstance(columns[0], int):
            columns = [self._hdrs[i] for i in columns]
        # numbers_col = OrderedDict()
        numbers = [0 for i in range(n)]
        for c in columns:
            for i in range(len(self._data[columns[0]])):
                numbers[int(self._data[c][i]) - 1] += 1
            # numbers_col[c] = numbers
        #analysis['COUNT_OCCUR_{}'.format(id0)] = OrderedDict({'name': 'count_number_of_responses_per_modalities', 'columns': ','.join(columns), 
        #         'results': ','. join(['(' +','.join(['nb_{}: {}'.format(i + 1, numbers_col[c][i]) for i in range(n)]) + ')' for c in columns]),
        #         'comment': comment})
        analysis['COUNT_OCCUR_{}'.format(id0)] = OrderedDict({'name': 'count_number_of_responses_per_modalities', 'columns': ','.join(columns), 
                 'results': '(' +', '.join(['{}'.format(numbers[i]) for i in range(n)]) + ')',
                 'comment': '{}. Modalites: {}'.format(comment, ','.join([str(i + 1) for i in range(n)]))})
        return analysis
                
if __name__ == '__main__':
    os.chdir('test')
    filename = r'reponses_questionnaire_memoire_sansnoms.csv'
    data = Dataset(filename, delimiter=';')
    columns_1 = [i - 1 for i in [2, 20]]
    analysis = data.mean_per_participant(columns_1, comment='Affrontement de la situation', id0='1')
    print(analysis.keys())
    columns_2 = [i - 1 for i in [13, 24] + [10, 19]]
    analysis = data.mean_per_participant(columns_2, comment='Resolution du probleme', id0='2')
    print(analysis.keys())
    columns_3 = [i - 1 for i in [5,14] + [9, 18] + [7, 27]]
    analysis = data.mean_per_participant(columns_3, comment='Recherche de soutien émotionnel', id0='3')
    print(analysis.keys())
    columns_4 = [i - 1 for i in [12, 25]]
    analysis = data.mean_per_participant(columns_4, comment='Auto-accusation', id0='4')
    columns_5 = [i - 1 for i in [11, 26]]
    analysis = data.mean_per_participant(columns_5, comment='Reevaluation positive', id0='5')
    columns_6 = [i - 1 for i in [8, 23] + [16, 28]]
    analysis = data.mean_per_participant(columns_6, comment='Minimisation de la menace', id0='6')
    columns_7 = [i - 1 for i in [3, 21] + [1, 17] + [6, 15] + [4, 22]]
    analysis = data.mean_per_participant(columns_7, comment='Evitement/fuite', id0='7')
    columns_8 = columns_1 + columns_2
    analysis = data.mean_per_participant(columns_8, comment='Coping tourne vers le probleme', id0='8')
    columns_9 = columns_3 + columns_4 + columns_5 + columns_6 + columns_7
    analysis = data.mean_per_participant(columns_9, comment='Coping tourne vers les emotions', id0='9')
    print(analysis.keys())
    #write_csv(analysis, filename.split('.')[0] + '_comp' + '.csv', append=False, delimiter=';')
    analysis = data.count_number_of_responses_per_modalities(columns_1, comment='Affrontement de la situation', id0='1')
    print(analysis.keys())
    analysis = data.count_number_of_responses_per_modalities(columns_2, comment='Resolution du probleme', id0='2')
    print(analysis.keys())
    analysis = data.count_number_of_responses_per_modalities(columns_3, comment='Recherche de soutien émotionnel', id0='3')
    print(analysis.keys())
    analysis = data.count_number_of_responses_per_modalities(columns_4, comment='Auto-accusation', id0='4')
    analysis = data.count_number_of_responses_per_modalities(columns_5, comment='Reevaluation positive', id0='5')
    analysis = data.count_number_of_responses_per_modalities(columns_6, comment='Minimisation de la menace', id0='6')
    analysis = data.count_number_of_responses_per_modalities(columns_7, comment='Evitement/fuite', id0='7')
    analysis = data.count_number_of_responses_per_modalities(columns_8, comment='Coping tourne vers le probleme', id0='8')
    analysis = data.count_number_of_responses_per_modalities(columns_9, comment='Coping tourne vers les emotions', id0='9')
    write_ini(analysis, filename, username='Mathieu Kergoat', date='20/11/1970')